from typing import Mapping
from pygame import Color, Surface, Vector2
from lib.theme import Theme
from utils import Vector2Int


class Cell:

    color: Color
    __is_wall: bool
    __theme: Theme

    def __init__(self, position: Vector2Int, theme: Theme, is_wall=False) -> None:
        self.__theme = theme
        self.color = theme.wall_color if is_wall else theme.path_color
        self.__is_wall = is_wall
        self.__position = position

    def draw(self, surface: Surface, position: Vector2Int, size: Vector2):
        surface.fill(self.color, (position.x * size.x,
                     position.y * size.y, size.x, size.y))

    @property
    def position(self) -> Vector2Int:
        return self.__position

    def __hash__(self) -> int:
        return hash(self.__position)

    def __eq__(self, value: object) -> bool:
        if not isinstance(value, Cell):
            return False
        return self.__position == value.__position

    def highlight(self):
        """Highlight the cell with the highlight color of the theme."""
        self.color = self.__theme.highlight_color

    @property
    def is_wall(self) -> bool:
        return self.__is_wall

    @is_wall.setter
    def is_wall(self, value: bool) -> None:
        """Set the cell to be a wall or a path and it's color accordingly."""
        self.color = self.__theme.wall_color if value else self.__theme.path_color
        self.__is_wall = value


class Grid:

    __size: Vector2Int
    __cells: Mapping[Vector2Int, Cell]
    __theme: Theme

    def __init__(self, size: Vector2Int, theme: Theme) -> None:
        self.__size = size
        self.__cells = {}
        self.__theme = theme
        self.__init_cells()

    @property
    def size(self) -> Vector2Int:
        return self.__size

    def __init_cells(self):
        for x in range(self.__size.x):
            for y in range(self.__size.y):
                pos = Vector2Int(x, y)
                self.__cells[pos] = Cell(pos, self.__theme)

    def get(self, key: Vector2Int) -> Cell:
        cell = self.__cells.get(key)
        if cell is None:
            raise KeyError(f"Cell out of bounds: {key}")
        return cell

    def all(self) -> list[Cell]:
        return list(self.__cells.values())

    def get_neighbors(self, key: Vector2Int) -> list[Cell]:
        neighbors = []
        positions = [
            Vector2Int(-1, 0),
            Vector2Int(1, 0),
            Vector2Int(0, -1),
            Vector2Int(0, 1)
        ]
        for position in positions:
            neighbor = Vector2Int(key.x + position.x, key.y + position.y)
            if neighbor.x >= 0 and neighbor.x < self.__size.x and neighbor.y >= 0 and neighbor.y < self.__size.y:
                neighbors.append(self.__cells[neighbor])

        return neighbors

    def position_in_bounds(self, position: Vector2Int) -> bool:
        return position.x >= 0 and position.x < self.__size.x and position.y >= 0 and position.y < self.__size.y

    def draw(self, surface: Surface, cell_size: Vector2):
        for x in range(self.__size.x):
            for y in range(self.__size.y):

                self.__cells[Vector2Int(x, y)].draw(
                    surface, Vector2Int(x, y), cell_size)
