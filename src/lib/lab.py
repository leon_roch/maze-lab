import pygame
from pygame.time import Clock
from pygame.display import Info
from typing import Callable, Tuple,  Union

from grid import Grid
from maze_generator import MazeGenerator
from lib.logibot import LogiBot
from lib.theme import DefaultTheme, Theme
from utils import Vector2Int


class Lab:
    """The main class for the Maze Lab simulation. This class is responsible for creating the simulation environment.

    Args:
        size (Union[Vector2Int, int]): The size of the grid. If an integer is provided, a square grid will be created.
        theme (Theme, optional): The theme of the simulation. Defaults to DefaultTheme.
        skip_maze_generation (bool, optional): If set to True, the maze generation will be skipped. Defaults to False.

    Methods:
        simulate: Starts the simulation with the provided simulation function.
    """

    __size: Vector2Int
    __skip_maze_generation: bool
    __theme: Theme

    def __init__(self, size: Union[Vector2Int, int], theme: Theme = DefaultTheme, skip_maze_generation: bool = False):
        if isinstance(size, int):
            size = Vector2Int(size, size)
        self.__size = size
        self.__skip_maze_generation = skip_maze_generation
        self.__theme = theme

    def simulate(self, simulation: Callable[[LogiBot, Vector2Int], Callable[[], bool]], loop: bool = False, tick_rate: int = None) -> None:
        """Starts the simulation with the provided simulation function.

        Args:
            simulation (Callable[[Robot, Vector2Int], Callable[[], bool]]): The simulation function that will control the robot. The function should return a tick function that will be called every frame.
            loop (bool, optional): If set to True, the simulation will restart after the target is found. Defaults to False.
            tick_rate (int, optional): The tick rate of the simulation. Default is adjusted based on the grid size.
        """
        pygame.init()
        pygame.display.set_caption("Maze Lab")
        icon = pygame.image.load("./assets/icon.png")
        pygame.display.set_icon(icon)

        size = self.__ideal_screen_size()
        screen = pygame.display.set_mode(size, pygame.RESIZABLE)

        clock = Clock()
        running = True
        shutdown = False

        if tick_rate is None:
            tick_rate = self.__adjust_tick_rate()

        grid = Grid(self.__size, self.__theme)

        start = Vector2Int()

        odder = Vector2Int(
            2 if grid.size.x % 2 == 0 else 1,
            2 if grid.size.y % 2 == 0 else 1
        )

        goal = Vector2Int(grid.size.x - odder.x, grid.size.y - odder.y)

        generator = MazeGenerator(grid)
        maze_generated = False
        generate_maze = generator.tick_generator(start)

        if self.__skip_maze_generation:
            generator.generate(start)
            maze_generated = True
            # draw the maze
            for cell in grid.all():
                if cell.is_wall:
                    cell.color = self.__theme.wall_color
                else:
                    cell.color = self.__theme.path_color

        # A big shout out to the creator of this robot image: https://opengameart.org/users/david-harrington
        robot = LogiBot(pygame.image.load("./assets/robot.png"), grid, start)

        simulation_tick = simulation(robot, goal)
        target_found = False

        screen.fill(self.__theme.wall_color)

        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                    shutdown = True

            [screenX, screenY] = screen.get_size()
            cellWidth = screenX // grid.size.x
            cellHeight = screenY // grid.size.y
            cell_size = Vector2Int(cellWidth, cellHeight)

            # entity drawing
            grid.draw(screen, cell_size)
            robot.draw(screen, cell_size)
            if maze_generated and not target_found:
                target_found = simulation_tick()
                if loop and target_found:
                    running = False

            if not self.__skip_maze_generation and not maze_generated:
                maze_generated = generate_maze()

            if maze_generated:
                grid.get(start).color = self.__theme.start_color
                grid.get(goal).color = self.__theme.exit_color

            pygame.display.flip()
            clock.tick(tick_rate)
        pygame.quit()
        if loop:
            if not shutdown:
                self.simulate(simulation, loop)

    def __adjust_tick_rate(self):
        if self.__size.x <= 10:
            return 20
        if self.__size.x <= 20:
            return 45
        if self.__size.x <= 40:
            return 60
        return 120

    def __ideal_screen_size(self) -> Tuple[int, int]:
        try:
            info = Info()
            smaller = min(info.current_w, info.current_h) - 100
            return (smaller, smaller)
        except:
            return (480, 480)
