from random import randint
from typing import Set

from grid import Cell, Grid
from utils import Vector2Int


class MazeGenerator:
    """Class to generate a maze using Prim's algorithm."""

    __grid: Grid

    def __init__(self, grid: Grid) -> None:
        self.__grid = grid

    def generate(self, start_pos: Vector2Int = Vector2Int()) -> None:

        # algorithm needs all cells to be walls initially
        for cell in self.__grid.all():
            cell.is_wall = True

        starting_cell = self.__grid.get(start_pos)
        starting_cell.is_wall = False

        walls: Set[Cell] = self.__get_neighbors(start_pos)

        while len(walls) > 0:

            wall = walls.pop(randint(0, len(walls) - 1))
            wall.is_wall = False

            neighbors = self.__get_neighbors(wall.position)

            path_neighbors = [
                neighbor for neighbor in neighbors if not neighbor.is_wall]

            if len(path_neighbors) == 0:
                continue

            neighbor_to_connect_to = path_neighbors[randint(
                0, len(path_neighbors) - 1)]

            in_between_position = Vector2Int(
                (wall.position.x + neighbor_to_connect_to.position.x) // 2,
                (wall.position.y + neighbor_to_connect_to.position.y) // 2
            )

            in_between_cell = self.__grid.get(in_between_position)
            in_between_cell.is_wall = False

            for neighbor in neighbors:
                if neighbor.is_wall and neighbor not in walls:
                    walls.append(neighbor)

    def tick_generator(self, start_pos: Vector2Int = Vector2Int()):

        # algorithm needs all cells to be walls initially
        for cell in self.__grid.all():
            cell.is_wall = True

        starting_cell = self.__grid.get(start_pos)
        starting_cell.is_wall = False

        walls: Set[Cell] = self.__get_neighbors(start_pos)

        def tick():
            if len(walls) == 0:
                return True

            wall = walls.pop(randint(0, len(walls) - 1))
            wall.is_wall = False

            neighbors = self.__get_neighbors(wall.position)

            path_neighbors = [
                neighbor for neighbor in neighbors if not neighbor.is_wall]

            if len(path_neighbors) == 0:
                return False

            neighbor_to_connect_to = path_neighbors[randint(
                0, len(path_neighbors) - 1)]

            in_between_position = Vector2Int(
                (wall.position.x + neighbor_to_connect_to.position.x) // 2,
                (wall.position.y + neighbor_to_connect_to.position.y) // 2
            )

            in_between_cell = self.__grid.get(in_between_position)
            in_between_cell.is_wall = False

            for neighbor in neighbors:
                if neighbor.is_wall and neighbor not in walls:
                    walls.append(neighbor)

            for cell in walls:
                cell.highlight()

            return False

        return tick

    def __get_neighbors(self, key: Vector2Int) -> list[Cell]:
        """Get the neighbors but only the ones that are 2 cells away."""
        neighbors = []
        positions = [
            Vector2Int(-2, 0),
            Vector2Int(2, 0),
            Vector2Int(0, -2),
            Vector2Int(0, 2)
        ]

        for position in positions:
            neighbor = Vector2Int(key.x + position.x, key.y + position.y)
            if neighbor.x >= 0 and neighbor.x < self.__grid.size.x and neighbor.y >= 0 and neighbor.y < self.__grid.size.y:
                neighbors.append(self.__grid.get(neighbor))

        return neighbors
