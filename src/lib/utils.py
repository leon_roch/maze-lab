import math


class Vector2Int:
    """A 2D integer vector class

    Attributes:
        x (int): The x coordinate of the vector
        y (int): The y coordinate of the vector
    """
    x: int
    y: int

    def __init__(self, x: int = 0, y: int = 0) -> None:
        self.x = x
        self.y = y

    def rotate_right(self, angle: float | int) -> 'Vector2Int':
        angle = math.radians(angle)  # Convert angle from degrees to radians
        new_x = int(self.x * math.cos(angle) - self.y * math.sin(angle))
        new_y = int(self.x * math.sin(angle) + self.y * math.cos(angle))
        return Vector2Int(new_x, new_y)

    def rotate_left(self, angle: float | int) -> 'Vector2Int':
        angle = math.radians(angle)
        new_x = int(self.x * math.cos(angle) + self.y * math.sin(angle))
        new_y = int(-self.x * math.sin(angle) + self.y * math.cos(angle))
        return Vector2Int(new_x, new_y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __eq__(self, other):
        if isinstance(other, Vector2Int):
            return self.x == other.x and self.y == other.y
        return False

    def __str__(self) -> str:
        return f"({self.x}, {self.y})"

    def __repr__(self) -> str:
        return f"Vector2Int({self.x}, {self.y})"
