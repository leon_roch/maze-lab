from pygame import Color


class Theme:
    __wall_color: Color
    __path_color: Color
    __start_color: Color
    __exit_color: Color
    __highlight_color: Color

    def __init__(self, wall_color: Color, path_color: Color, start_color: Color, exit_color: Color, highlight_color: Color):
        self.__wall_color = wall_color
        self.__path_color = path_color
        self.__start_color = start_color
        self.__exit_color = exit_color
        self.__highlight_color = highlight_color

    @property
    def wall_color(self) -> Color:
        return self.__wall_color

    @property
    def path_color(self) -> Color:
        return self.__path_color

    @property
    def start_color(self) -> Color:
        return self.__start_color

    @property
    def exit_color(self) -> Color:
        return self.__exit_color

    @property
    def highlight_color(self) -> Color:
        return self.__highlight_color


DefaultTheme: Theme = Theme(wall_color=(0, 40, 69),
                            path_color=(255, 255, 255),
                            start_color=(252, 114, 63),
                            exit_color=(246, 73, 117),
                            highlight_color=(0, 207, 204))
