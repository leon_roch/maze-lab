from pygame import Surface, Vector2
from pygame.transform import scale as scale_surface

from grid import Grid
from utils import Vector2Int


class LogiBot:
    """Class to represent LogiBot in the maze

    Attributes:
        position (Vector2Int): The position of LogiBot on the grid

    Methods:
        move (position: Vector2Int): Move LogiBot to a new position on the grid
        get_neighboring_paths () -> list[Vector2Int]: Get the neighboring paths of LogiBot
    """

    __sprite: Surface
    __position: Vector2Int
    """The position of LogiBot on the grid"""
    __grid: Grid

    def __init__(self, sprite: Surface, grid: Grid, position: Vector2Int = Vector2Int()) -> None:
        self.__sprite = sprite
        self.__grid = grid
        self.__position = position

    def draw(self, surface: Surface, size: Vector2):
        """Internal method to draw LogiBot on the screen. This method should not be called directly!"""
        if self.__sprite.get_size() != [size.x, size.y]:
            self.__sprite = scale_surface(self.__sprite, [size.x, size.y])

        surface.blit(self.__sprite, (self.__position.x *
                     size.x, self.__position.y * size.y))

    @property
    def position(self) -> Vector2Int:
        """Get the position of LogiBot on the grid"""
        return self.__position

    def move(self, position: Vector2Int) -> None:
        """Move LogiBot to a new position on the grid. The new position must be next to the current position.

        Parameters:
            position (Vector2Int): The new position of LogiBot

        Raises:
            MoveError: If the position is not next to the current position, out of bounds, or a wall
        """
        if not self.__is_valid_move(position):
            raise MoveError(
                f"Invalid move\nCurrent position: {self.__position}\nNew position: {position}")
        if not self.__grid.position_in_bounds(position):
            raise MoveError("Out of bounds")
        if self.__grid.get(position).is_wall:
            raise MoveError("Cannot move to a wall")
        self.__position = position
        self.__grid.get(self.__position).highlight()

    def get_neighboring_paths(self) -> list[Vector2Int]:
        """Get the neighboring paths of LogiBot

        Returns:
            list[Vector2Int]: The positions of the neighboring paths
        """
        neighbours = self.__grid.get_neighbors(self.__position)
        return [neighbour.position for neighbour in neighbours if not neighbour.is_wall]

    def __is_valid_move(self, position: Vector2Int) -> bool:
        if abs(self.__position.x - position.x) == 1 and self.__position.y == position.y:
            return True
        if abs(self.__position.y - position.y) == 1 and self.__position.x == position.x:
            return True
        return False


class MoveError(Exception):

    def __init__(self, message: str) -> None:
        super().__init__(message)
