from lib import Lab, LogiBot, Vector2Int


def algorithm(logibot: LogiBot, exit: Vector2Int):

    # initialize your stack or whatever you need

    def tick():
        # Todo: Implement your algorithm here
        # Return True if the exit is found
        # Return False if the exit is not found yet
        pass

    return tick


lab = Lab(10)
lab.simulate(algorithm)
