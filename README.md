# Maze Lab

![Trailer](./assets/trailer.gif)

Meet LogiBot, a robot that needs your help to find the exit of a maze. You can help him by implementing a maze solving algorithm in python.

Maze Lab is a simple game where you can implement algorithms to help LogiBot find the exit of a maze. The purpose of this Project is to practice the implementation of algorithms in a fun and interactive way.

## Prerequisites

To run this project you need to have pyenv or python 3.10.0 installed.

I highly recommend using pyenv to manage your python versions. To install pyenv, run the following command:

### [Linux (Ubuntu)](https://github.com/pyenv/pyenv)

```bash
curl https://pyenv.run | bash
```

### [Windows](https://github.com/pyenv-win/pyenv-win)

```powershell
Invoke-WebRequest -UseBasicParsing -Uri "https://raw.githubusercontent.com/pyenv-win/pyenv-win/master/pyenv-win/install-pyenv-win.ps1" -OutFile "./install-pyenv-win.ps1"; &"./install-pyenv-win.ps1"; rm "./install-pyenv-win.ps1"
```

## Installation

After cloning the repository, navigate to the project directory and run the following command:

```
pyenv install 3.10.0
pyenv local 3.10.0
pip install -r requirements.txt
```

Now you're all set to use the lab.

## Usage

write your code in the `src/main.py` file and run the following command to test your implementation:

```bash
python3 ./src/main.py
```

## Example

Let's implement the depth-first search algorithm to solve the maze. \
In the `src/main.py` file, it should look like this:

```python
from lib import Lab, LogiBot, Vector2Int


def algorithm(logibot: LogiBot, exit: Vector2Int):

    # initialize your stack or whatever you need

    def tick():
        # Todo: Implement your algorithm here
        # Return True if the exit is found
        # Return False if the exit is not found yet
        pass

    return tick


lab = Lab(10)
lab.simulate(algorithm)
```

Let's implement the `algorithm` function:

```python
def algorithm(logibot: LogiBot, exit: Vector2Int):

    visited = [logibot.position]
    stack = [logibot.position]

    def tick():
        pass

    return tick
```

To keep track of the visited cells, we will use a list called `visited` that will store the positions of the cells that LogiBot has already visited. \
We will also use a stack to keep track of the movements that LogiBot has made. \
Finally, we return the `tick` function that will be called every frame.

We get the position of LogiBot using `robot.position` and we add it to the `visited` list. \

Now let's implement the `tick` function:

```python
def algorithm(logibot: LogiBot, exit: Vector2Int):

    visited = [logibot.position]
    stack = [logibot.position]

    def tick():
        neighbors = logibot.get_neighboring_paths()
        neighbors = [
            neighbor for neighbor in neighbors if neighbor not in visited]

        if len(neighbors) > 0:
            stack.append(neighbors[0])
            visited.append(neighbors[0])

            logibot.move(neighbors[0])

            return logibot.position == exit

        stack.pop()
        logibot.move(stack[-1])

        return False

    return tick
```

First, we get the neighboring walkable cells using the `robot.get_neighboring_paths()` method. \
Then we filter the neighbors to get only the cells that LogiBot has not visited yet. \
If there are neighbors that LogiBot has not visited yet, we move LogiBot to the first neighbor in the list. \
If the neighbor is the exit, we return `True` to indicate that the exit has been found. \
If there are no neighbors that LogiBot has not visited yet, we pop the last cell from the stack and move LogiBot to the last cell in the stack. \
Finally, we return `False` to indicate that the exit has not been found yet.

By running from the terminal:

```bash
python3 ./src/main.py
```

You should see LogiBot finding the exit of the maze using the depth-first search algorithm. \
You can implement other algorithms to solve the maze, such as the breadth-first search algorithm or the Wall Follower algorithm.

## Documentation

### `Lab`

The `Lab` class is responsible for simulating the maze and LogiBot. \
It has the following methods:

#### `__init__(self, size: int | Vector2Int, theme: Theme = DefaultTheme(), skip_maze_generation: bool = False)`

- `size`: The size of the maze. If it is an integer, the maze will be a square with the given size. If it is a `Vector2Int`, the maze will have the given width and height.
- `theme`: The theme of the maze. The default theme is `DefaultTheme`. You can create your own theme by creating your own Theme instance.
- `skip_maze_generation`: If `True`, the maze will not be generated visually. The default value is `False`.

#### `simulate(self, algorithm: Callable[[LogiBot, Vector2Int], Callable[[], bool]], loop: bool = False, tick_rate: int = None)`

- `algorithm`: The algorithm that will be used to solve the maze. The algorithm should return a function that will be called every frame. The function should return `True` if the exit is found and `False` if the exit is not found yet.
- `loop`: If `True`, the simulation will create a new maze every time the exit is found. The default value is `False`.
- `tick_rate`: The rate at which the algorithm will be called. The default is adjusted to the size of the maze.

### `LogiBot`

The `LogiBot` class is responsible for moving LogiBot in the maze. \
It has the following properties:

- `position`: The position of LogiBot in the maze.

It has the following methods:

#### `move(self, position: Vector2Int)`

The LogiBot can only move to neighboring cells (up, down, left, right). \
If the given position is not a neighboring cell, an error will be raised.

- `position`: The position where LogiBot will move.

#### `get_neighboring_paths(self) -> List[Vector2Int]`

Returns the neighboring cells that LogiBot can move to.

### `Vector2Int`

The `Vector2Int` class is a simple class that represents a 2D vector with integer values. \
It has the following properties:

- `x`: The x value of the vector.
- `y`: The y value of the vector.

It has the following methods:

#### `rotate_right(self, angle: float | int) -> Vector2Int`

Returns a new `Vector2Int` instance that is the result of rotating the vector to the right by the given angle.

- `angle`: The angle in degrees that the vector will be rotated.

#### `rotate_left(self, angle: float | int) -> Vector2Int`

Returns a new `Vector2Int` instance that is the result of rotating the vector to the left by the given angle.

- `angle`: The angle in degrees that the vector will be rotated.

### `Theme`

The `Theme` class is a simple class that represents the theme of the maze. \

It has the following properties:

- `wall_color`: The color of the walls of the maze.
- `path_color`: The color of the path of the maze.
- `start_color`: The color of the start of the maze.
- `exit_color`: The color of the exit of the maze.
- `highlight_color`: The color of the highlighted cells of the maze. The highlighted cells are the cells that LogiBot has visited.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## Acknowledgements

The character (LogiBot) used in this project was created by [David Harrington](https://opengameart.org/users/david-harrington). I am deeply grateful for his contribution, especially as the character is free of any copyright restrictions. This has greatly enriched the project, and I want to express my sincere appreciation.

## License

This project is licensed under the [All Rights Reserved License](./LICENSE).
